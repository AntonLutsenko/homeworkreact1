import { Component } from "react";
import "./App.scss";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import modalParams from "./components/Modal/modalParams";

class App extends Component {
	constructor() {
		super();
		this.state = {
			showModal: false,
			modalValue: {
				modalId: 0,
				modalHeader: "title",
				modalText: "text",
				backgroundColor: "",
				closeButton: false,
			},
		};
	}

	adjustModal = (e) => {
		const dataId = e.target.id;
		const modalData = modalParams.find((element) => element.id === dataId);

		this.setState({
			showModal: true,
			modalValue: {
				dataId: modalData.id,
				header: modalData.header,
				text: modalData.text,
				text2: modalData.text2,
				backgroundColor: modalData.backgroundColor,
				closeButton: modalData.closeButton,
				action1: modalData.action1,
				action2: modalData.action2,
				classNameBtn1: modalData.classNameBtn1,
				classNameBtn2: modalData.classNameBtn2,
			},
		});
		// console.log(this.state.showModal);

		// console.log(this.state.modalParams.text);
	};

	hideModal = () => {
		// console.log(1);
		// console.log(this.state.showModal);
		this.setState({ showModal: !this.state.showModal });
		// console.log(this.state.showModal);
	};

	render() {
		return (
			<>
				<Button
					dataId={"first"}
					className={"button"}
					text={"Open first modal"}
					backgroundColor={"rgb(255, 235, 205)"}
					onClick={this.adjustModal}
				/>
				<Button
					dataId={"second"}
					className={"button"}
					text={"Open second modal"}
					backgroundColor={"rgb(209, 249, 166)"}
					onClick={this.adjustModal}
				/>
				{this.state.showModal && (
					<Modal
						onClick={this.hideModal}
						closeButton={this.state.modalValue.closeButton}
						header={this.state.modalValue.header}
						text={this.state.modalValue.text}
						text2={this.state.modalValue.text2}
						backgroundColor={this.state.modalValue.backgroundColor}
						actions={
							<>
								<Button
									className={this.state.modalValue.classNameBtn1}
									text={this.state.modalValue.action1}
								/>
								<Button
									className={this.state.modalValue.classNameBtn2}
									text={this.state.modalValue.action2}
								/>
							</>
						}
					/>
				)}
			</>
		);
	}
}

export default App;
