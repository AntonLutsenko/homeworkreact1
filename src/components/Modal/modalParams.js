const modalParams = [
	{
		id: "first",
		header: "Do you want to delete this file?",
		text: `Once you delete this file, it won't be possible to undo this
      action`,
		text2: `Are you sure you want to delete it?`,
		backgroundColor: "rgba(255, 0, 0, 0.8)",
		closeButton: true,
		action1: "Ok",
		action2: "Cancel",
		classNameBtn1: "button btn-ok",
		classNameBtn2: "button btn-cancel",
	},
	{
		id: "second",
		header: "ong established fact that a reader will ?",
		text: "ince the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap i ",
		text2: `Where can I get some?`,
		backgroundColor: "rgba(76, 177, 76, 0.8)",
		closeButton: true,
		action1: "Ok",
		action2: "Cancel",
		classNameBtn1: "button btn-ok-second",
		classNameBtn2: "button btn-cancel-second",
	},
];

export default modalParams;
